export type BeerType = {
    id: number; 
    name: string;
    image_url: string;
    tagline: string;
    first_brewed: string;
    description: string;
    abv: number;
    ibu: number;
    ebc: number;
    srm: number;
    food_pairing: [string];
  };